class Library
@@lib = "Seattle Public Library"
attr_reader :books, :shelves
include AddShelves
include AddBooks
include KnowBooks

  	def know_shelves
	    puts "#{@shelves}"
 	end
end

class Shelf
attr_reader :books
include AddShelves
include KnowBooks
end

class Book
attr_reader :books, :shelves
include AddBooks

	def enshelf (bookAssign, shelfAssign)
		@bookAssign = bookAssign
		@shelfAssign = shelfAssign
		puts "#{bookAssign} is now on shelf #{shelfAssign}"
	end

	def unshelf (bookUnnasign)
		@bookOut = bookUnnasign
		puts "#{@bookOut} no longer is assigned to a shelf"
	end
end

module AddShelves
attr_accessor :shelves
@@shelves = []

	def initialize (:shelves)
		@shelfName = :shelves
		@@shelves.push[@shelfName]
	end
end

module AddBooks
attr_accessor :books
@@books=[]

	def initialize (:books)
		@bookName = :books
		@@books.push[@bookName]
	end
end

module KnowBooks
@knowBooks << @@books

	def know_books
		puts "#{@knowBooks}"
	end
end


Book.AddBooks.new(books: "it")
Book.AddBooks.new(books: "The Road")

Shelf.AddShelves.new(shelves: "horror")
Shelf.AddBooks.new("it")

Library.AddShelves.new(shelves: "horror")
Library.AddBooks.new(books: "it")
Library.AddBooks.new(books: "The Road")

Library.know_books




